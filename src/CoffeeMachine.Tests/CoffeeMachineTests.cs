﻿using CoffeeMachine.Drinks;
using CoffeeMachine.Messages;
using MediatR;
using NSubstitute;
using Xunit;

namespace CoffeeMachine.Tests
{
    public class CoffeeMachineTests
    {
        [Fact]
        public void When_Send_An_Order_With_Correct_Money_Should_Call_DrinkMakerHandler_And_DrinkHistoryHandler()
        {
            var mediator = Substitute.For<IMediator>();
            var coffeeMachine = new CoffeeMachine(mediator);
            var drink = new Chocolate(1);
            var order = Order.CreateNewOrder(drink, 10);
            coffeeMachine.SendOrder(order);

            mediator.Received().Send(Arg.Any<Order>());
            mediator.Received().Publish(Arg.Any<MessageDrinkHistory>());
        }
    }
}