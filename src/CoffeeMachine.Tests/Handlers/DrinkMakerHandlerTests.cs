﻿using CoffeeMachine.Handlers;
using CoffeeMachine.Tests.Internal;
using FluentAssertions;
using Xunit;

namespace CoffeeMachine.Tests.Handlers
{
    public class DrinkMakerHandlerTests
    {
        [Theory]
        [InlineData(TypeDrink.Coffee, 1, 0.3, "M:0.3")]
        [InlineData(TypeDrink.Chocolate, 1, 0.1, "M:0.4")]
        [InlineData(TypeDrink.Tea, 1, 0.3, "M:0.1")]
        [InlineData(TypeDrink.OrangeJuice, 1, 0.2, "M:0.4")]
        public void DrinkMakerHandler_When_Money_Are_Not_Enough_Should_Return_A_Missing_Money_Message(TypeDrink typeDrink, int sugar, decimal currentMoney, string expectedResponse)
        {
            var order = Utilities.FactoryOrder(typeDrink, sugar, currentMoney);
            var drinkMakerHandler = new DrinkMakerHandler();
            var response= drinkMakerHandler.Handle(order);
            response.ShouldBeEquivalentTo(expectedResponse);
        }

        [Theory]
        [InlineData(TypeDrink.Coffee, 0, 10, "C::")]
        [InlineData(TypeDrink.Coffee, 2, 10, "C:2:0")]
        [InlineData(TypeDrink.Chocolate, 0, 10, "H::")]
        [InlineData(TypeDrink.Chocolate, 2, 10, "H:2:0")]
        [InlineData(TypeDrink.Tea, 0, 10, "T::")]
        [InlineData(TypeDrink.Tea, 2, 10, "T:2:0")]
        [InlineData(TypeDrink.OrangeJuice, 0, 10, "O::")]
        [InlineData(TypeDrink.OrangeJuice, 2, 10, "O:2:0")]
        public void DrinkMakerHandler(TypeDrink typeDrink, int sugar, decimal currentMoney, string expectedResponse)
        {
            var order = Utilities.FactoryOrder(typeDrink, sugar, currentMoney);
            var drinkMakerHandler = new DrinkMakerHandler();
            var response = drinkMakerHandler.Handle(order);
            response.ShouldBeEquivalentTo(expectedResponse);
        }
    }
}
