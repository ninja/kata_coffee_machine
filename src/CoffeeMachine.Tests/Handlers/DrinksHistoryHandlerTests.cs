﻿using System.Linq;
using CoffeeMachine.Drinks;
using CoffeeMachine.Handlers;
using CoffeeMachine.Messages;
using FluentAssertions;
using Xunit;

namespace CoffeeMachine.Tests.Handlers
{
    public class DrinksHistoryHandlerTests
    {
        [Fact]
        public void GetReport_Check_Result()
        {
            var drinkHistoryHandler = new DrinksHistoryHandler();
            drinkHistoryHandler.Handle( new MessageDrinkHistory( new OrangeJuice(1)));
            drinkHistoryHandler.Handle(new MessageDrinkHistory(new OrangeJuice(1)));
            drinkHistoryHandler.Handle(new MessageDrinkHistory(new Chocolate(1)));
            drinkHistoryHandler.Handle(new MessageDrinkHistory(new Tea(1)));
            drinkHistoryHandler.Handle(new MessageDrinkHistory(new Tea(1)));
            drinkHistoryHandler.Handle(new MessageDrinkHistory(new Tea(1)));

            var report = drinkHistoryHandler.GetReport();
            report.Should().NotBeNull();
            report.Count.Should().Be(3);
            report.Any(x => x == "O:2:1.2").Should().BeTrue();
            report.Any(x => x == "H:1:0.5").Should().BeTrue();
            report.Any(x => x == "T:3:1.2").Should().BeTrue();
        }
    }
}