﻿using System;
using FluentAssertions;
using Xunit;

namespace CoffeeMachine.Tests
{
    public class OrderTests
    {
        [Fact]
        public void CreateNewOrder_With_Drink_Null_Should_Return_ArgumentNullException()
        {
            Action action = () => Order.CreateNewOrder(null, 10);
            action.ShouldThrow<ArgumentNullException>();
        }
    }
}
