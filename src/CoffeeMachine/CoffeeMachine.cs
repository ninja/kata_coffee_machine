﻿using CoffeeMachine.Messages;
using MediatR;

namespace CoffeeMachine
{
    public class CoffeeMachine
    {
        private readonly IMediator _mediator;

        public CoffeeMachine(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async void SendOrder(Order order)
        {
            var reponseDrinkMaker = await _mediator.Send(order);
            if (!reponseDrinkMaker.StartsWith("M:"))
                _mediator.Publish(new MessageDrinkHistory(order.Drink));
        }
    }
}
