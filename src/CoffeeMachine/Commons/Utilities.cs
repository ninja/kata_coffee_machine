﻿using System.Globalization;

namespace CoffeeMachine.Commons
{
    public static class Utilities
    {
        public static CultureInfo GetCurrentCulture => new CultureInfo("en-US");

    }
}
