﻿
namespace CoffeeMachine.Drinks
{
    public class Coffee : Drink
    {
        public Coffee(int sugar) : base("C", sugar, 0.6M)
        {
        }
    }
}
