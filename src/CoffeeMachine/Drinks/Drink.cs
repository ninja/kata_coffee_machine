﻿using System;

namespace CoffeeMachine.Drinks
{
    public abstract class Drink
    {
        protected Drink( string code, int sugar, decimal price)
        {
            if( string.IsNullOrWhiteSpace(code))
                throw new ArgumentNullException(nameof(code));
            if (sugar < 0 || sugar > 2)
                throw new ArgumentException("The sugar must have a value beetwen 0 and 2");
            if (price < 0)
                throw new ArgumentException("The price can not be negative");
            this.Code = code;
            this.Sugar = sugar;
            this.Price = price;
        }

        public int Sugar { get; }
        public decimal Price { get; }
        public string Code { get; }
        public bool IsExtraHot { get; private set; }

        public Drink AddExtraHot()
        {
            this.IsExtraHot = true;
            return this;
        }

        public override string ToString()
        {
            var code = Code;
            if( this.IsExtraHot)
                code = $"{this.Code}h";

            var sugar = Sugar > 0 ? Sugar.ToString() : string.Empty;
            var stick = Sugar > 0 ? "0" : string.Empty;
            return $"{code}:{sugar}:{stick}";
        }
    }
}
