﻿using CoffeeMachine.Commons;
using MediatR;

namespace CoffeeMachine.Handlers
{
    public class DrinkMakerHandler : IRequestHandler<Order, string>
    {
        public string Handle(Order order)
        {
            if (order.Drink.Price > order.CurrentMoney)
                return ShowMissingMoney(order.Drink.Price - order.CurrentMoney);
            return order.Drink.ToString();
        }

        private string ShowMissingMoney(decimal missingMoney)
        {
            return $"M:{missingMoney.ToString(Utilities.GetCurrentCulture)}";
        }
    }
}