﻿using System.Collections.Generic;
using System.Linq;
using CoffeeMachine.Commons;
using CoffeeMachine.Drinks;
using CoffeeMachine.Messages;
using MediatR;

namespace CoffeeMachine.Handlers
{
    public class DrinksHistoryHandler : INotificationHandler<MessageDrinkHistory>
    {
        private readonly List<Drink> _drinksHistory;
        public DrinksHistoryHandler()
        {
            _drinksHistory = new List<Drink>();
        }

        public void Handle(MessageDrinkHistory notification)
        {
            _drinksHistory.Add( notification.Drink);
        }

        public List<string> GetReport()
        {
            return _drinksHistory.GroupBy(x => x.Code).Select(g => $"{g.Key}:{g.Count()}:{g.Sum(x => x.Price).ToString(Utilities.GetCurrentCulture)}").ToList();
        }
    }
}