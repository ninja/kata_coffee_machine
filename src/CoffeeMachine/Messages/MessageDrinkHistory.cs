﻿using CoffeeMachine.Drinks;
using MediatR;

namespace CoffeeMachine.Messages
{
    public class MessageDrinkHistory : INotification
    {
        public MessageDrinkHistory(Drink drink)
        {
            this.Drink = drink;
        }

        public Drink Drink { get; }
    }
}
