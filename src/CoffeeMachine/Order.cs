﻿using System;
using CoffeeMachine.Drinks;
using MediatR;

namespace CoffeeMachine
{
    public class Order : IRequest<string>
    {
        public Drink Drink { get; }
        public decimal CurrentMoney { get; }

        private Order(Drink drink, decimal currentMoney)
        {
            this.Drink = drink;
            this.CurrentMoney = currentMoney;
        }

        public static Order CreateNewOrder(Drink drink, decimal currentMoney)
        {
            if( drink == null)
                throw new ArgumentNullException(nameof(drink));
            return new Order(drink, currentMoney);
        }
    }
}