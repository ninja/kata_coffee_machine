﻿using CoffeeMachine.Handlers;
using CoffeeMachine.Messages;
using MediatR;
using Ninject;
using Ninject.Extensions.Conventions;

namespace CoffeeMachine
{
    public sealed class Startup
    {
        private static volatile Startup _instance;
        public StandardKernel Kernel { get; }
        private static readonly object SyncRoot = new object();

        private Startup()
        {
            Kernel = InitKernel();
        }

        public static Startup Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new Startup();
                    }
                }

                return _instance;
            }
        }

        private static StandardKernel InitKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind(scan => scan.FromAssemblyContaining<IMediator>().SelectAllClasses().BindDefaultInterface());
            kernel.Bind<INotificationHandler<MessageDrinkHistory>>().To<DrinksHistoryHandler>();
            kernel.Bind<IRequestHandler<Order, string>>().To<DrinkMakerHandler>();
            kernel.Bind<SingleInstanceFactory>().ToMethod(ctx => t => ctx.Kernel.TryGet(t));
            kernel.Bind<MultiInstanceFactory>().ToMethod(ctx => t => ctx.Kernel.GetAll(t));
            return kernel;
        }
    }
    
}
