﻿using System.Linq;
using FluentAssertions;
using SimplyCoffeeMachine.Drinks;
using SimplyCoffeeMachine.Tests.Internal;
using Xunit;

namespace SimplyCoffeeMachine.Tests
{
    public class CoffeeMachineTests
    {
        [Theory]
        [InlineData(TypeDrink.Coffee, 1, 0.3, "M:0.3")]
        [InlineData(TypeDrink.Chocolate, 1, 0.1, "M:0.4")]
        [InlineData(TypeDrink.Tea, 1, 0.3, "M:0.1")]
        [InlineData(TypeDrink.OrangeJuice, 1, 0.2, "M:0.4")]
        public void When_Money_Are_Not_Enough_Should_Return_A_Missing_Money_Message(TypeDrink typeDrink, int sugar, decimal currentMoney, string expectedMessage)
        {
            var order = Utilities.FactoryOrder(typeDrink, sugar, currentMoney);
            var drinkMaker = new DrinkMaker();
            var coffeeMachine = new CoffeeMachine(drinkMaker);
            coffeeMachine.SendOrder( order);
            drinkMaker.Commands.Contains(expectedMessage).Should().BeTrue();
        }

        [Fact]
        public void GetReport_Check_Result()
        {
            var drinkMaker = new DrinkMaker();
            var coffeeMachine = new CoffeeMachine(drinkMaker);
            coffeeMachine.SendOrder( Order.CreateNewOrder(new OrangeJuice(1), 0.6M));
            coffeeMachine.SendOrder(Order.CreateNewOrder(new OrangeJuice(1), 0.6M));
            coffeeMachine.SendOrder(Order.CreateNewOrder(new Chocolate(1), 0.5M));
            coffeeMachine.SendOrder(Order.CreateNewOrder(new Tea(1), 0.4M));
            coffeeMachine.SendOrder(Order.CreateNewOrder(new Tea(1), 0.4M));
            coffeeMachine.SendOrder(Order.CreateNewOrder(new Tea(1), 0.4M));

            var report = coffeeMachine.GetReport();
            report.Should().NotBeNull();
            report.Count.Should().Be(3);
            report.Any(x => x == "O:2:1.2").Should().BeTrue();
            report.Any(x => x == "H:1:0.5").Should().BeTrue();
            report.Any(x => x == "T:3:1.2").Should().BeTrue();
        }
    }
}