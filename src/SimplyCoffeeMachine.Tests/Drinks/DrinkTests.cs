﻿using System;
using Xunit;
using FluentAssertions;
using SimplyCoffeeMachine.Drinks;
using SimplyCoffeeMachine.Tests.Internal;

namespace SimplyCoffeeMachine.Tests.Drinks
{
    public class DrinkTests
    {
        [Fact]
        public void Give_A_Drink_With_More_Two_Sugar_Should_Return_ArgumentException()
        {
            Action action = () => new Coffee(3);
            action.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void Give_A_Drink_With_Less_Zero_Sugar_Should_Return_ArgumentException()
        {
            Action action = () => new Coffee(-1);
            action.ShouldThrow<ArgumentException>();
        }

        [Theory]
        [InlineData(TypeDrink.Coffee, 1, "Ch:1:0")]
        [InlineData(TypeDrink.Chocolate, 1, "Hh:1:0")]
        [InlineData(TypeDrink.Tea, 1, "Th:1:0")]
        [InlineData(TypeDrink.OrangeJuice, 1, "Oh:1:0")]
        public void AddExtraHot(TypeDrink typeDrink, int sugar, string expectedCode)
        {
            var drink = Utilities.FactoryDrink(typeDrink, sugar).AddExtraHot();
            drink.ToString().ShouldBeEquivalentTo(expectedCode);
        }

        [Theory]
        [InlineData(TypeDrink.Coffee, 0, "C::")]
        [InlineData(TypeDrink.Coffee, 2, "C:2:0")]
        [InlineData(TypeDrink.Chocolate, 0, "H::")]
        [InlineData(TypeDrink.Chocolate, 2, "H:2:0")]
        [InlineData(TypeDrink.Tea, 0, "T::")]
        [InlineData(TypeDrink.Tea, 2, "T:2:0")]
        [InlineData(TypeDrink.OrangeJuice, 0, "O::")]
        [InlineData(TypeDrink.OrangeJuice, 2, "O:2:0")]
        public void ToString(TypeDrink typeDrink, int sugar, string expectedCode)
        {
            var drink = Utilities.FactoryDrink(typeDrink, sugar);
            drink.ToString().ShouldBeEquivalentTo( expectedCode);
        }

        [Theory]
        [InlineData(TypeDrink.Coffee, 1, 0.6)]
        [InlineData(TypeDrink.Chocolate, 1, 0.5)]
        [InlineData(TypeDrink.Tea, 1, 0.4)]
        [InlineData(TypeDrink.OrangeJuice, 1, 0.6)]
        public void Check_Price(TypeDrink typeDrink, int sugar, decimal expectedPrice)
        {
            var drink = Utilities.FactoryDrink(typeDrink, sugar);
            drink.Price.Should().Be(expectedPrice);
        }
    }
}
