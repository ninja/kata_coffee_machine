﻿using System;
using SimplyCoffeeMachine.Drinks;

namespace SimplyCoffeeMachine.Tests.Internal
{
    public enum TypeDrink
    {
        Coffee,
        Tea,
        Chocolate,
        OrangeJuice
    }

    internal static class Utilities
    {
        public static Drink FactoryDrink(TypeDrink typeDrink, int sugar)
        {
            switch (typeDrink)
            {
                case TypeDrink.Coffee:
                    return new Coffee(sugar);
                case TypeDrink.Tea:
                    return new Tea(sugar);
                case TypeDrink.Chocolate:
                    return new Chocolate(sugar);
                case TypeDrink.OrangeJuice:
                    return new OrangeJuice(sugar);
                default:
                    throw new ArgumentOutOfRangeException(nameof(typeDrink), typeDrink, null);
            }
        }

        public static Order FactoryOrder(TypeDrink typeDrink, int sugar, decimal currentMoney)
        {
            return Order.CreateNewOrder(FactoryDrink(typeDrink, sugar), currentMoney);
        }
    }
}
