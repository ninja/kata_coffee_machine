﻿using System;
using FluentAssertions;
using SimplyCoffeeMachine.Drinks;
using Xunit;

namespace SimplyCoffeeMachine.Tests
{
    public class OrderTests
    {
        [Fact]
        public void CreateNewOrder_With_Drink_Null_Should_Return_ArgumentNullException()
        {
            Action action = () => Order.CreateNewOrder(null, 10);
            action.ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void CreateNewOrder_Check_Order()
        {
            const decimal currentMoney = 10;
            var drink = new Coffee(1);
            var order = Order.CreateNewOrder(drink, currentMoney);
            order.Drink.Should().NotBeNull();
            order.CurrentMoney.Should().Be(currentMoney);
            order.Drink.Should().BeOfType<Coffee>();
        }
    }
}
