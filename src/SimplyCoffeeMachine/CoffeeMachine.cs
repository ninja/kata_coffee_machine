﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SimplyCoffeeMachine.Drinks;
using SimplyCoffeeMachine.Interfaces;

namespace SimplyCoffeeMachine
{
    public class CoffeeMachine
    {
        private readonly IDrinkMaker _drinkMaker;
        private readonly List<Drink> _drinksHistory;
        private static CultureInfo CultureInfo => new CultureInfo("en-US");

        public CoffeeMachine(IDrinkMaker drinkMaker)
        {
            _drinkMaker = drinkMaker;
            _drinksHistory = new List<Drink>();
        }

        public void SendOrder(Order order)
        {
            if (order.Drink.Price > order.CurrentMoney)
            {
                ShowMissingMoney(order.Drink.Price - order.CurrentMoney);
                return;
            }
            _drinkMaker.ProcessCommand( order.ToString());
            _drinksHistory.Add( order.Drink);
        }

        private void ShowMissingMoney(decimal missingMoney)
        {
            var message = missingMoney.ToString(CultureInfo);
            _drinkMaker.ProcessCommand($"M:{message}");
        }

        public IList<string> GetReport()
        {
            return _drinksHistory.GroupBy(x => x.Code).Select(g => $"{g.Key}:{g.Count()}:{g.Sum(x => x.Price).ToString(CultureInfo)}").ToList();
        }
    }
}