﻿using System.Collections.Generic;
using SimplyCoffeeMachine.Interfaces;

namespace SimplyCoffeeMachine
{
    public class DrinkMaker : IDrinkMaker
    {
        public readonly List<string> Commands;

        public DrinkMaker()
        {
            Commands = new List<string>();    
        }

        public void ProcessCommand(string command)
        {
            Commands.Add(command);
        }
    }
}