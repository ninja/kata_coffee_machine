﻿namespace SimplyCoffeeMachine.Drinks
{
    public class Chocolate : Drink
    {
        public Chocolate(int sugar) : base("H", sugar, 0.5M)
        {
        }
    }
}
