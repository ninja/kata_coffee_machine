﻿namespace SimplyCoffeeMachine.Drinks
{
    public class OrangeJuice : Drink
    {
        public OrangeJuice(int sugar) : base("O", sugar, 0.6M)
        {
        }
    }
}
