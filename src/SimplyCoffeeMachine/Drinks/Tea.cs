﻿namespace SimplyCoffeeMachine.Drinks
{
    public class Tea : Drink
    {
        public Tea(int sugar) : base("T", sugar, 0.4M)
        {
        }
    }
}
