﻿
namespace SimplyCoffeeMachine.Interfaces
{
    //Last Step kata
    public interface IBeverageQuantityChecker
    {
        bool IsEmpty(string drink);
    }
}
