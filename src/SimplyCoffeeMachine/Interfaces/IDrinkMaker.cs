﻿namespace SimplyCoffeeMachine.Interfaces
{
    public interface IDrinkMaker
    {
        void ProcessCommand(string command);
    }
}
