﻿
namespace SimplyCoffeeMachine.Interfaces
{
    //Last Step kata
    public interface IEmailNotifier
    {
        void NotifyMissingDrink(string drink);
    }
}