﻿using System;
using SimplyCoffeeMachine.Drinks;

namespace SimplyCoffeeMachine
{
    public class Order
    {
        public Drink Drink { get; private set; }
        public decimal CurrentMoney { get; private set; }

        private Order(Drink drink, decimal currentMoney)
        {
            this.Drink = drink;
            this.CurrentMoney = currentMoney;
        }

        public static Order CreateNewOrder(Drink drink, decimal currentMoney)
        {
            if (drink == null)
                throw new ArgumentNullException(nameof(drink));
            return new Order(drink, currentMoney);
        }
    }
}
